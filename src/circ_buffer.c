/****************************************************************************
 * File:	circ_buffer.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/common/src/circ_buffer.c,v 1.8 2008/02/08 13:40:02 ahoms Exp $
 * Project:	ESRF Linux driver framework
 * Description:	Circular buffer source code
 * Author(s):	A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: circ_buffer.c,v $
 * Revision 1.8  2008/02/08 13:40:02  ahoms
 * Ported to x86_64 architecture
 *
 * Revision 1.7  2007/10/23 15:07:48  ahoms
 * Support Linux 2.4 through esrfversion.h.
 * Check for valid src/dest pointers when copying data.
 * Do not depend on scdxipci headers; independent debug structure.
 *
 * Revision 1.6  2007/08/23 17:20:40  ahoms
 * Ported to Linux 2.6:
 *  Added xfer_flags to circ_buffer_getc/putc, introduced CIRC_BUFFER_KTHREAD.
 *  Using long long arithmetic instead of double in timeout calculation.
 *
 * Revision 1.5  2006/11/27 19:37:30  ahoms
 * Corrected timeout handling to expire when the user requested.
 * Added xfer_data struct. containing all the info concerning the xfer.
 * Introduced terminator search but it's NOT IMPLEMENTED YET!
 *
 * Revision 1.4  2006/10/02 12:57:06  ahoms
 * Using standard wait_event_interruptible_timeout return code.
 * Do not block again when the first xfer timed out.
 *
 * Revision 1.3  2005/12/02 09:52:27  ahoms
 * Moved wait_event_interruptible_timeout to separate header file.
 * Return timeout as negative error: ETIMEDOUT.
 * Fixed bug in circ_buffer_getc confusing return character 0xff with -1.
 *
 * Revision 1.2  2005/09/13 09:19:35  ahoms
 * Removed BLOCK from xfer_flags; using timeout values
 * CIRC_BUFFER_NO_BLOCK and CIRC_BUFFER_BLOCK_FOREVER.
 * Allow checking if bytes has been transmitted or received,
 * and block, if not and requested, without transferring.
 *
 * Revision 1.1  2005/08/15 21:16:33  ahoms
 * Implemented read/write agents with independent wait queues;
 * xfer flag CIRC_BUFFER_WAKEUP is no longer necessary.
 * Added circ_buffer_flush.
 * Improved timeout and interrupt management, and putc/getc debug.
 *
 * Revision 1.0  2005/08/14 13:51:19  ahoms
 * Initial revision
 *
 ****************************************************************************/

#include "circ_buffer.h"
#include "esrfdebug.h"

#include <linux/slab.h>
#include <linux/ktime.h>
#include <asm/uaccess.h>

#define circ_buffer_lock(lock, flags) \
	do { (flags) = 0; if (lock) spin_lock_irqsave(lock, flags); } while (0)
#define circ_buffer_unlock(lock, flags) \
	do { if (lock) spin_unlock_irqrestore(lock, flags); } while (0)

#define empty_xfer(xfer)	((xfer)->data_len == 0)

/*--------------------------------------------------------------------------
 * RCS id
 *--------------------------------------------------------------------------*/

static char rcs_id[] __attribute__((unused)) = 
	"$Header: /segfs/bliss/cvs/driver/ESRF/common/src/circ_buffer.c,v 1.8 2008/02/08 13:40:02 ahoms Exp $";


/*--------------------------------------------------------------------------
 * debug stuff
 *--------------------------------------------------------------------------*/

DECLARE_DEBUG_LEVEL(circ_buffer_debug_level, CIRC_BUFFER_DEB_ERRORS);

#define DEB_HEADING		"circ_buffer"

#define CIRC_BUFFER_DEB_ALWAYS	0

#define DEB_ALWAYS(fmt, ...)	\
	DPRINTK(CIRC_BUFFER_DEB_ALWAYS, fmt , ## __VA_ARGS__)
#define DEB_ERRORS(fmt, ...)	\
	DPRINTK(CIRC_BUFFER_DEB_ERRORS, fmt , ## __VA_ARGS__)
#define DEB_PARAMS(fmt, ...)	\
	DPRINTK(CIRC_BUFFER_DEB_PARAMS, fmt , ## __VA_ARGS__)
#define DEB_XFER(fmt, ...)	\
	DPRINTK(CIRC_BUFFER_DEB_XFER,   fmt , ## __VA_ARGS__)


/*--------------------------------------------------------------------------
 * time helpers
 *--------------------------------------------------------------------------*/

static inline 
unsigned long circ_buffer_correct_timeout_us(ktime_t t0, 
					     unsigned long timeout_us,
					     int *timedout)
{
	int block = (timeout_us != CIRC_BUFFER_NO_BLOCK);
	int block_forever = (timeout_us == CIRC_BUFFER_BLOCK_FOREVER);
	unsigned long elap_us, tout_us = timeout_us;
	ktime_t t1;
	if (block && !block_forever) {
		t1 = ktime_get();
		elap_us = ktime_to_ns(ktime_sub(t1, t0)) / 1000;
		if (elap_us >= timeout_us) {
			*timedout = 1;
			return 0;
		}
		tout_us -= elap_us;
	}
	*timedout = 0;
	return tout_us;
}


/*--------------------------------------------------------------------------
 * circ_buffer_init:	prepares a serial buffer
 *--------------------------------------------------------------------------*/

int circ_buffer_init_agent(struct circ_buffer_agent *agent)
{
	init_waitqueue_head(&agent->wq);
	agent->ptr = 0;
	agent->wait_count = 0;
	return 0;
}

int circ_buffer_init(struct circ_buffer *buff, unsigned long len)
{
	FENTRY("circ_buffer_init");

	buff->len = 0;
	buff->xfer = 0;
	circ_buffer_init_agent(&buff->read);
	circ_buffer_init_agent(&buff->write);

	DEB_PARAMS("Allocating %ld bytes for circ_buffer @ 0x%p\n", len, buff);
	buff->buffer = (char *) kmalloc(len, GFP_KERNEL);
	if (buff->buffer == NULL) {
		DEB_ERRORS("Error allocating %ld bytes for circ_buffer\n", 
			   len);
		return -ENOMEM;
	}

	buff->len = len;
	return 0;
}

/*--------------------------------------------------------------------------
 * circ_buffer_cleanup:	cleanup the buffer
 *--------------------------------------------------------------------------*/

int circ_buffer_cleanup(struct circ_buffer *buff)
{
	FENTRY("circ_buffer_cleanup");

	wake_up_interruptible(&buff->read.wq);
	wake_up_interruptible(&buff->write.wq);

	if (buff->buffer != NULL) {
		kfree(buff->buffer);
		DEB_PARAMS("Freeing %ld bytes in circ_buffer @ 0x%p\n", 
			   buff->len, buff);
	}
	buff->buffer = NULL;
	buff->len = 0;
	circ_buffer_init_agent(&buff->read);
	circ_buffer_init_agent(&buff->write);
	return 0;
}

/*--------------------------------------------------------------------------
 * circ_buffer_bytes:	return the number of bytes in buffer. 
 *			if free=1, return free bytes
 *--------------------------------------------------------------------------*/

int circ_buffer_bytes(struct circ_buffer *buff, int free, spinlock_t *lock)
{
	int bytes;
	unsigned long flags;

	circ_buffer_lock(lock, flags);
	bytes = buff->write.ptr - buff->read.ptr;
	circ_buffer_unlock(lock, flags);
	if (bytes < 0)
		bytes += buff->len;
	return free ? ((int) buff->len - 1 - bytes) : bytes;
}



/*--------------------------------------------------------------------------
 * circ_buffer_check_block:	check the nr of bytes that can be transferred.
 *				block if none and asked for. since the event 
 *				to wait for uses wait_count, it's important
 *				to keep the lock until properly set. 
 *				never ask to wait for more than 50% of
 *				the buffer. in case of a blocking with
 *				no byte; block until the buffer is empty 
 *				(write) or has at least one byte (read).
 *				return the nr of bytes to xfer,
 *				or 0 if NO_BLOCK, or -ETIMEDOUT if timeout,
 *				or -ERESTARTSYS if interrupted
 *--------------------------------------------------------------------------*/

int circ_buffer_check_block(struct circ_buffer *buff, 
			    struct circ_buffer_xfer_data *xfer, 
			    spinlock_t *lock, unsigned long timeout_us)
{
	int write, block, just_check, waited, end, kthread, freebytes, inst;
	long waitret;
	unsigned long can_xfer, ticks, flags, len = xfer->data_len;
	struct circ_buffer_agent *agent;

	FENTRY("circ_buffer_check_block");

#define wokenup ((agent->wait_count == 0) || \
		 (kthread && kthread_should_stop()))

	write   = circ_buffer_flag(xfer, CIRC_BUFFER_WRITE);
	kthread = circ_buffer_flag(xfer, CIRC_BUFFER_KTHREAD);
	block = (timeout_us != CIRC_BUFFER_NO_BLOCK);
	just_check = empty_xfer(xfer);
	agent = write ? &buff->write : &buff->read;

	inst = (xfer->term_len > 0);
	if (inst) {
		DEB_ERRORS("terminator search not implemented yet!\n");
		return -ENOSYS;
		// buff->xfer = xfer;
	}

	xfer->timedout = waited = waitret = 0;
	while (1) {
		circ_buffer_lock(lock, flags);
		freebytes = just_check ? 0 : write;
		can_xfer = circ_buffer_bytes(buff, freebytes, NULL);

		if (!just_check && (can_xfer > len))
			can_xfer = len;
		end = (just_check && write) ? !can_xfer : can_xfer;
		if (end || waited || !block)
			break;

		if (just_check) {
			if (!write)
				can_xfer = 1;
		} else {
			can_xfer = buff->len / 2;
			can_xfer = (len < can_xfer) ? len : can_xfer;
		}
		agent->wait_count = can_xfer;

		circ_buffer_unlock(lock, flags);

		ticks = circ_buffer_timeout(timeout_us);
		waitret = wait_event_interruptible_timeout(agent->wq, wokenup, 
							   ticks);
		if (waitret == 0) {
			waitret = -ETIMEDOUT;
			xfer->timedout = 1;
		}
		waited = 1;
	}
	circ_buffer_unlock(lock, flags);

	if (inst)
		buff->xfer = NULL;

	if ((can_xfer == 0) && (waitret < 0)) {
		DEB_PARAMS("%s\n", (waitret == -ERESTARTSYS) ? "Interrupted" :
			                                       "Timed out");
		return waitret;
	}

#undef wokenup

	return can_xfer;
}


/*--------------------------------------------------------------------------
 * circ_buffer_copy_data:	do the real copy, depending if it is 
 *				read or write to/from user or kernel space.
 *--------------------------------------------------------------------------*/
int circ_buffer_copy_data(struct circ_buffer *buff, 
			  struct circ_buffer_xfer_data *xfer)
{
	char *readp, *writep;
	unsigned long len = xfer->data_len;
	int ret, write;

	FENTRY("circ_buffer_copy_data");

	if (buff->buffer == NULL) {
		DEB_ERRORS("Invalid NULL circ_buffer pointer!\n");
		return -EINVAL;
	} else if (xfer->data == NULL) {
		DEB_ERRORS("Invalid NULL xfer buffer pointer!\n");
		return -EINVAL;
	}
		
	write = circ_buffer_flag(xfer, CIRC_BUFFER_WRITE);
	writep =  write ? (buff->buffer + buff->write.ptr) : xfer->data;
	readp  = !write ? (buff->buffer + buff->read.ptr)  : xfer->data;
	if (circ_buffer_flag(xfer, CIRC_BUFFER_USER)) {
		if (write)
			ret = copy_from_user(writep, readp, len);
		else
			ret = copy_to_user(writep, readp, len);
		if (ret != 0) {
			DEB_ERRORS("Error copying %ld bytes %s user space\n", 
				   len, write ? "from" : "to");
			return -EACCES;
		}
	} else
		memcpy(writep, readp, len);

	return 0;
}

/*--------------------------------------------------------------------------
 * circ_buffer_update_ptr:	update the corresponding buffer ptr after
 *				the xfer. wakeup the process waiting if asked
 *				for and the expected nr of bytes was 
 *				transferred; return 1 if so
 *--------------------------------------------------------------------------*/

int circ_buffer_update_ptr(struct circ_buffer *buff, 
			   struct circ_buffer_xfer_data *xfer, spinlock_t *lock)
{
	unsigned long write, flags, wakeup, len = xfer->data_len;
	volatile unsigned long *ptr;
	struct circ_buffer_agent *agent;
	
	write = circ_buffer_flag(xfer, CIRC_BUFFER_WRITE);
	ptr = write ? &buff->write.ptr : &buff->read.ptr;
	agent = write ? &buff->read : &buff->write;

	circ_buffer_lock(lock, flags);
	*ptr += len;
	*ptr %= buff->len;

	wakeup = 0; 
	if (agent->wait_count > 0) {
		if (agent->wait_count <= len)
			agent->wait_count = 0;
		else
			agent->wait_count -= len;
		wakeup = (agent->wait_count == 0);
	}

	circ_buffer_unlock(lock, flags);

	if (wakeup)
		wake_up_interruptible(&agent->wq);

	return wakeup;
}


/*--------------------------------------------------------------------------
 * circ_buffer_xfer:	transfer len bytes from (write=1) or to (write=0) data.
 *			xfer to/from user space (usr_spc=1). if lock, use
 *			it in buffer pointers arithmetics.
 *			return nr. of bytes transferred, 0 if timeout or
 *			-ERESTARTSYS if interrupted. in a blocking transfer
 *			of 0 bytes, wait until the buffer is empty (write) or
 *			has at least one byte (read) or a timeout.
 *--------------------------------------------------------------------------*/

int circ_buffer_xfer(struct circ_buffer *buff, 
		     struct circ_buffer_xfer_data *xfer, 
		     spinlock_t *lock, unsigned long timeout_us)
{
	unsigned long tot_xfer, ptr, write, len, tout_us;
	int ret, block, just_check;
	struct circ_buffer_xfer_data this_xfer, aux_xfer = *xfer;
	ktime_t t0;

	write = circ_buffer_flag(xfer, CIRC_BUFFER_WRITE);
	block = (timeout_us != CIRC_BUFFER_NO_BLOCK);
	just_check = empty_xfer(xfer);
	t0 = ktime_get();
	tout_us = timeout_us;

	tot_xfer = ret = 0;
	while ((!empty_xfer(&aux_xfer) || (write && block) || just_check) &&
	       !aux_xfer.timedout) {
		ret = circ_buffer_check_block(buff, &aux_xfer, lock, tout_us);
		if ((ret <= 0) || empty_xfer(&aux_xfer)) // != just_check
			break;

		len = ret;
		ptr = write ? buff->write.ptr : buff->read.ptr;
		if (len > buff->len - ptr)
			len = buff->len - ptr;

		this_xfer = aux_xfer;
		this_xfer.data_len = len;
		ret = circ_buffer_copy_data(buff, &this_xfer);
		if (ret < 0)
			return ret;

		tot_xfer += len;
		aux_xfer.data += len;
		aux_xfer.data_len -= len;

		circ_buffer_update_ptr(buff, &this_xfer, lock);

		tout_us = circ_buffer_correct_timeout_us(t0, timeout_us, 
							 &aux_xfer.timedout);
	}

	xfer->timedout = aux_xfer.timedout;

	return (tot_xfer > 0) ? (int) tot_xfer : ret;
}


/*--------------------------------------------------------------------------
 * circ_buffer_putc/getc
 *--------------------------------------------------------------------------*/

char *get_char_repr(char ch) 
{
	static char c[2] = {0, 0};
	switch ((c[0] = ch)) {
	case '\n': return "\\n";
	case '\r': return "\\r";
	case '\t': return "\\t";
	case '\0': return "\\0";
	default:   return c;
	}
}

int circ_buffer_putc(struct circ_buffer *buff, int chr, spinlock_t *lock, 
		     int flags, unsigned long timeout_us)
{
	char repr[10], c = chr & 0xff;
	int ret;
	struct circ_buffer_xfer_data xfer;

	FENTRY("circ_buffer_putc");

	flags |= CIRC_BUFFER_WRITE | CIRC_BUFFER_KERNEL;
	circ_buffer_xfer_data_init(&xfer, &c, 1, NULL, 0, flags);
	ret = circ_buffer_xfer(buff, &xfer, lock, timeout_us);
	ret = (ret == 1) ? 0 : -1;

	sprintf(repr, (ret >= 0) ? "'%s'" : "None", get_char_repr(c));
	DEB_XFER("buff=0x%p, chr=%s, ret=%d\n", buff, repr, ret);
	return ret;
}

int circ_buffer_getc(struct circ_buffer *buff, spinlock_t *lock, 
		     int flags, unsigned long timeout_us)
{
	char repr[10], c;
	int ret;
	struct circ_buffer_xfer_data xfer;
		
	FENTRY("circ_buffer_getc");

	flags |= CIRC_BUFFER_READ | CIRC_BUFFER_KERNEL;
	circ_buffer_xfer_data_init(&xfer, &c, 1, NULL, 0, flags);
	ret = circ_buffer_xfer(buff, &xfer, lock, timeout_us);
	ret = (ret == 1) ? (unsigned char) c : -1;

	sprintf(repr, (ret >= 0) ? "'%s'" : "None", get_char_repr(c));
	DEB_XFER("buff=0x%p, chr=%s, ret=%d\n", buff, repr, ret);
	return ret;
}


/*--------------------------------------------------------------------------
 * circ_buffer_flush:	flush the buffer content and return the number of
 *			bytes thrown away
 *--------------------------------------------------------------------------*/

int circ_buffer_flush(struct circ_buffer *buff, spinlock_t *lock)
{
	unsigned long flags, nrbytes;

	circ_buffer_lock(lock, flags);

	nrbytes = circ_buffer_bytes(buff, CIRC_BUFFER_READ, NULL);
	wake_up_interruptible(&buff->read.wq);
	wake_up_interruptible(&buff->write.wq);
	circ_buffer_init_agent(&buff->read);
	circ_buffer_init_agent(&buff->write);

	circ_buffer_unlock(lock, flags);

	return nrbytes;
}

