/****************************************************************************
 * File:	app_framework.c
 * $Header: /segfs/bliss/cvs/driver/ESRF/common/src/app_framework.c,v 1.20 2017/11/15 12:05:24 ahoms Exp $
 * Project:	ESRF Linux driver framework
 * Description:	Library test program framework
 * Author(s):	A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: app_framework.c,v $
 * Revision 1.20  2017/11/15 12:05:24  ahoms
 * * Fixed warnings/errors with gcc-4.9
 *
 * Revision 1.19  2016/03/07 13:59:48  ahoms
 * * Igore input lines starting with # in app_framework
 *
 * Revision 1.18  2009/03/16 14:35:55  ahoms
 * Added context_cleanup
 *
 * Revision 1.17  2009/02/26 16:12:25  ahoms
 * Include single quotes in the search of next command (;); moved code to get_next_cmd
 *
 * Revision 1.16  2009/02/09 19:46:31  kirov
 * + new command to allow users to add custom lines in the EDF file header
 * + the image library is initialized only when it is going to be used
 *
 * Revision 1.15  2008/09/20 23:03:39  ahoms
 * Little cosmetic change
 *
 * Revision 1.14  2008/06/04 15:34:39  kirov
 * "Run cmd_script_file" command TEST implementation.
 *
 * Revision 1.13  2008/05/26 15:47:38  kirov
 * Minor implementation change: use standard functions.
 *
 * Revision 1.12  2008/05/26 09:26:04  kirov
 * Added <TAB> completion for the commands and corresponding parameter display.
 *
 * Revision 1.11  2008/03/12 16:43:11  papillon
 *
 * Correct read_num_escape function for hexa string
 *
 * Revision 1.10  2007/10/23 15:05:47  ahoms
 * Printing only 7-bit ASCII characters in dump_data
 *
 * Revision 1.9  2007/08/02 12:25:48  ahoms
 * Proper exit of exec_application main loop when using input thread
 *
 * Revision 1.8  2007/07/12 09:55:43  ahoms
 * Using strtoll to fully decode 32-bit integer parameters
 *
 * Revision 1.7  2007/02/14 10:36:15  ahoms
 * Alloc also initializes the context, so out/err can be used immediately.
 * Modified init_context to return an error if app functions are invalid.
 *
 * Revision 1.6  2007/02/01 14:14:20  ahoms
 * Using implementation-independ. type app_context_t, added alloc/free functions
 * Using out/err output functions; flush on every access.
 * Added program arguments to init_context; decode --interactive argument.
 *
 * Revision 1.5  2007/01/13 12:30:38  ahoms
 * Added system Shell function
 *
 * Revision 1.4  2006/10/23 09:01:13  ahoms
 * Modified quit_app to properly end the application
 *
 * Revision 1.3  2006/10/06 17:53:51  ahoms
 * Increased the input buffer length to 1024 bytes
 *
 * Revision 1.2  2006/10/03 08:33:51  ahoms
 * Support main loop poll functions by means of a separate input thread.
 * Added optional parameters and long command help description.
 * Added Sleep command with usec resolution; added generic dump_data helper.
 * Added readline functionality, including persistent history.
 * Allow several commands in the same line separated by ";".
 *
 * Revision 1.1  2005/08/21 23:47:12  ahoms
 * Help menu works with long parameter list functions
 *
 * Revision 1.0  2005/08/14 14:00:14  ahoms
 * Initial revision
 *
 ****************************************************************************/

#include "app_framework.h"
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>
#include <ctype.h>
#include <stdarg.h>
#include <limits.h>
#include <readline/readline.h>
#include <readline/history.h>

#define WHITE_SPC		" \t\n\r"
#define setstrlen(s, l)		do { (s)[l] = '\0'; } while (0)

#define DEFAULT_SYSTEM_TICK	10000	// usec

struct app_context {
	char			*name;
	char			 hist_fname[128];
	FILE			*in, *out, *err;
	FILE			*cmdin;
	char			*input_name;
	int			 interactive;
	int			 depth;
	struct app_context	*parent;
	struct funct_data	*funcs;
	err_funct		*err_funct;
	void			*data;
	poll_funct	       **poll_funcs;
	int			 nr_poll_funcs;
	int			 poll_sleep_usec;
	void			*input_data;
	char			 cmd_buffer[MAX_STR_LEN];
	char			*next_cmd;
	int			 app_end;
};

struct input_data {
	pthread_t		 thread;
	pthread_cond_t		 event;
	pthread_mutex_t		 signal_lock;
	int			 ready;
	int			 ret;
	struct funct_data 	*fdata; 
	union param		 pars[MAX_NR_PARAM];
};

#define context_input_data(c)	((struct input_data *) (c)->input_data)

#define elapsed_usec(l, n) \
	(((n)->tv_sec - (l)->tv_sec) * 1000000 + ((n)->tv_usec - (l)->tv_usec))

static struct funct_data framework_funcs[] = {
	{"Sleep",	do_sleep,	"Sleep with usec resolution",
	 {{"sec",	PARAM_FLOAT, 	"Sleep time (in sec)"}, {NULL}}},
	{"Run",		run_script,	"Executes an script",
	 {{"scr_fname", PARAM_STR, 	"Script file name"}, {NULL}}},
	{"Shell",	system_shell,	"Executes a system shell command",
	 {{"cmd",       PARAM_STR, 	"Shell command"}, {NULL}}},
	{"Abort",	NULL,		"Aborts the current script",
	 {{NULL}}},
	{"Help",	print_help,	"Print this help",	
	 {{"command",   PARAM_STR_OPT, 	"Specific command"}, {NULL}}},
	{"Quit",	quit_app,	"Quit the application",	{{NULL}}},
	{NULL}
};

static char *param_type_name[NR_PARAM_TYPES] = {
	"character", "integer", "float", "string"
};

int is_interactive(struct app_context *context)
{
	return context->interactive;
}

char *bin_repr(char *buffer, unsigned long val)
{
	unsigned long bit;
	char *ptr = buffer;

	for (bit = 1UL << 31; bit != 0; bit >>= 1, ptr++) {
		if (bit & 0x08888888)
			*ptr++ = ' ';
		*ptr = (val & bit) ? '1' : '0';
	}
	*ptr = '\0';
	return buffer;
}


static struct funct_data *_app_functions=NULL; /* Only one instance possible! */


/* Generator function for command completion.  STATE lets us know whether
   to start from scratch; without any state (i.e. STATE == 0), then we
   start at the top of the list. */
char * command_generator (const char *text, int state)
{
	static int list_index1, list_index2, len;
	char *name;

	/* If this is a new word to complete, initialize now.  This includes
	   saving the length of TEXT for efficiency, and initializing the index
	   variables to 0. */
	if (!state)
	{
		list_index1 = 0;
		list_index2 = 0;
		len = strlen (text);
	}

	/* Return the next name which partially matches from the framework 
	   command list. */
	while( (name = framework_funcs[list_index1].name) )
	{
		list_index1++;

		if (strncasecmp (name, text, len) == 0)
			return (strdup(name));
	}

	/* Return the next name which partially matches from the application 
	   command list. */
	while( (name = _app_functions[list_index2].name) )
	{
		list_index2++;

		if (strncasecmp (name, text, len) == 0)
			return (strdup(name));
	}

	/* If no names matched, then return NULL. */
	return ((char *)NULL);
}


/* Count the words in the "line" before position "pos", including the one
   pointed by "pos"; "space" indicates if "pos" is on a white space or not */
int cmd_word_number( const char *line, int pos, int *space )
{
	int len, count;

	*space = 0;

	if( NULL==line || (pos < 0) || (pos > (len=strlen(line))) )
		return -1;

	if( !len ) 
		return 0;

	if( pos == len ) {
		if( isspace(line[pos-1]) ) 
			*space = 1;
		--pos;
	} else {
		if( pos && isspace(line[pos]) && isspace(line[pos-1]) )
			*space = 1;
	}

	count = 0;
	while( pos > 0 ) {
		if( !isspace(line[pos]) && isspace(line[pos-1]) )
			++count;
		--pos;
	}
	if( !isspace(line[0]) )
		++count;

	return count;
}


/* Attempt to complete on the contents of TEXT.  START and END bound the
   region of rl_line_buffer that contains the word to complete.  TEXT is
   the word to complete.  We can use the entire contents of rl_line_buffer
   in case we want to do some simple parsing.  Return the array of matches,
   or NULL if there aren't any. */
char ** cmd_completion (const char *text, int start, int end)
{
	char **matches=NULL;
	int wn, space;

	wn = cmd_word_number( rl_line_buffer, end, &space );

	if ((wn+space) <= 1) {
		/* If this word is at the start of the line, then it is a 
		   command to complete. */
		matches = rl_completion_matches (text, command_generator);
	} else {
		struct funct_data *found=NULL;
		unsigned int i, len;
		char *name, *first;

		/* Isolate the first word */
		first = rl_line_buffer;
		first += strspn(rl_line_buffer, WHITE_SPC);
		len = strcspn(first, WHITE_SPC);

		/* Search for the first word among the commands */
		for( i=0; (name = framework_funcs[i].name); i++ )
		{
			if ( (strlen(name) == len) 
			  && (strncasecmp(first, name, len) == 0) ) {
				found = &(framework_funcs[i]);
				break;
			}
		}
		if( !found ) {
			for( i=0; (name = _app_functions[i].name); i++ )
			{
				if ( (strlen(name) == len) 
				  && (strncasecmp(first, name, len) == 0) ) {
					found = &(_app_functions[i]);
					break;
				}
			}
		}

		/* Print the corresponding parameter */
		if( found && found->params ) {
			unsigned int w=wn+space-2;

			for( i=0; i<w && found->params[i].name; i++ );

			matches = (char **) malloc( 3*sizeof(char *) );
			matches[0] = strdup( text );
			if( (i == w) && found->params[i].name ) {
				len = strlen( found->params[i].name )
				    + strlen( found->params[i].desc ) + 8;
				matches[1] = (char *) malloc(len*sizeof(char));
				sprintf( matches[1], "  %s     %s", 
				         found->params[i].name,
				         found->params[i].desc );
			} else if( (i == w) && space ) {
				matches[1] = 
				    strdup("  <ENTER>     Execute the command");
			} else {
				matches[1] = strdup("  Parameter not found!");
			}
			matches[2] = NULL;
		}
	}

	/* If this function sets the rl_attempted_completion_over variable to 
	   a non-zero value, Readline will not perform its default completion 
	   even if this function returns no matches. */
	rl_attempted_completion_over = 1;

	return (matches);
}

		
int init_cmd_completion( struct funct_data *app_funcs )
{
	if( app_funcs ) {
		rl_attempted_completion_function = cmd_completion;

		_app_functions = app_funcs;

		return 0;
	}
	return -1;
}


app_context_t alloc_context(char *name, int argc, char *argv[])
{
	struct app_context *context;
        FILE *hist;
        char line[512], *p, *fname;
        int i, len;

	len = sizeof(*context);
	context = (struct app_context *) calloc(1, len);
	if (context == NULL) {
		fprintf(stderr, "Error allocating context (%d bytes)\n", len);
		return NULL;
	}
	
        context->name           = name;
        context->in             = stdin;
        context->out            = stdout;
        context->err            = stderr;
	context->cmdin		= stdin;
	context->interactive	= isatty(fileno(context->cmdin));
	context->input_name	= "(stdin)";
	context->depth		= 0;
	context->parent		= NULL;
	context->funcs		= NULL;
	context->err_funct	= NULL;
	context->poll_funcs	= NULL;
	context->data		= NULL;
	context->next_cmd	= NULL;

	for (i = 0; i < argc; i++)
		if (strcmp(argv[i], "--interactive") == 0)
			context->interactive = 1;

	fname = context->hist_fname;
	sprintf(fname, "%s/.%s_history", getenv("HOME"), context->name);
	hist = fopen(fname, "rt");
	if (hist != NULL) {
		while (fgets(line, 512, hist) != NULL) {
			if ((p = strchr(line, '\n')) != NULL)
				*p = 0;
			add_history(line);
		}
		fclose(hist);
	}

	return context;
}

void free_context(app_context_t context)
{
	free(context);
}


int init_context(struct app_context *context, struct funct_data *app_funcs, 
		 err_funct *err_funct, void *data)
{
	if (app_funcs == NULL) {
		context_err(context, "Invalid NULL app_funcs\n");
		return -1;
	}

	context->funcs		= app_funcs;
	context->err_funct	= err_funct;
	context->data		= data;

	init_cmd_completion( app_funcs );

	return 0;
}
		
int init_context_poll(struct app_context *context, poll_funct **poll_funcs,
		      int nr_poll_funcs, int poll_sleep_usec)
{
	context->poll_funcs	 = poll_funcs;
	context->nr_poll_funcs	 = nr_poll_funcs;
	context->poll_sleep_usec = poll_sleep_usec;
	return 0;
}

void cleanup_context(struct app_context *context)
{
	context->funcs		 = NULL;
	context->err_funct	 = NULL;
	context->data		 = NULL;
	context->poll_funcs	 = NULL;
	context->nr_poll_funcs	 = 0;
	context->poll_sleep_usec = 0;
}

int set_poll_sleep_time(struct app_context *context, int poll_sleep_usec)
{
	int prev_sleep = context->poll_sleep_usec;
	if (poll_sleep_usec >= 0)
		context->poll_sleep_usec = poll_sleep_usec;
	return prev_sleep;
}

void *context_app_data(struct app_context *context)
{
	return context->data;
}

int context_vprintf(FILE *stream, char *format,  va_list ap)
{
	int ret, fret;

	ret = vfprintf(stream, format, ap);
	if (ret >= 0) {
		fret = fflush(stream);
		if (fret < 0)
			ret = fret;
	}
			
	return ret;
}

int context_out(struct app_context *context, char *format, ...)
{
	va_list ap;
	int ret;

	va_start(ap, format);
	ret = context_vprintf(context->out, format, ap);
	va_end(ap);
	return ret;
}

int context_err(struct app_context *context, char *format, ...)
{
	va_list ap;
	int ret;

	va_start(ap, format);
	ret = context_vprintf(context->err, format, ap);
	va_end(ap);
	return ret;
}



int read_num_escape(char **endp, int base)
{
	int i, hexa = (base == 16);
	char orig, bchar, chr, max_num = hexa ? '9' : '7';

	for (i = chr = 0; i < (hexa ? 2 : 3); i++) {
		orig = *++(*endp);
		if ((orig >= '0') && (orig <= max_num))
			bchar = '0';
		else if (hexa && (orig >= 'A') && (orig <= 'F')) 
			bchar = 'A' - 0x0a;
		else if (hexa && (orig >= 'a') && (orig <= 'f')) 
			bchar = 'a' - 0x0a;
		else
			break;

		chr <<= hexa ? 4 : 3;
		chr += (orig - bchar);
	}

	return chr;
}

int conv_param(char *token, struct funct_param *fparam, union param *val)
{
	char quotes, chr, orig, *writep, *endp = token;

	switch (fparam->type & PARAM_TYPE_MASK) {
	case PARAM_CHAR:
		val->c = *endp++; break;
	case PARAM_INT:
		val->i = strtoll(token, &endp, 0) & ULONG_MAX; break;
	case PARAM_FLOAT:
		val->f = strtod(token, &endp); break;
	default:
		quotes = 0;
		writep = val->s;
		if ((*endp == '\'') || (*endp == '"'))
			quotes = *endp++;
		while ((chr = *endp) != 0) {
			if (chr == '\\') {
				orig = chr = *++endp;
				switch (chr) {
				case ' ':  chr = ' ';  break;
				case 'n':  chr = '\n'; break;
				case 't':  chr = '\t'; break;
				case 'r':  chr = '\r'; break;
				case '\\': chr = '\\'; break;
				case '0':
				case '1':
				case '2':
					endp--;
					chr = read_num_escape(&endp, 8); 
					break;
				case 'x':
					chr = read_num_escape(&endp, 16);
					break;
				default:
					if (!quotes || (chr != quotes))
						*writep++ = '\\';
				}
				if (orig == 0)
					break;
			} else if (quotes && (chr == quotes)) {
				quotes = 0;
				endp++;
				continue;
			} else if (!quotes && (strchr(WHITE_SPC, chr) != NULL))
				break;
			*writep++ = chr;
			endp++;
		}
		val->len = writep - val->s;
		if (quotes)
			return -2;
	}

	if (*endp && (strchr(WHITE_SPC, *endp) == NULL))
		return -1;

	return endp - token;
}

int context_get_line(struct app_context *context, char *buffer, int len)
{
	char *line, *ptr, *prompt;
	int use_readline;
	FILE *hist;

	prompt = "Type 'Help' for help >> ";
	use_readline = isatty(fileno(context->cmdin));
	if (use_readline) {
		line = readline(prompt);
		if (line && *line) {
			add_history(line);
			hist = fopen(context->hist_fname, "at");
			fputs(line, hist);
			if (strchr(line, '\n') == NULL)
				fputc('\n', hist);
			fclose(hist);
		}
	} else {
		if (is_interactive(context))
			context_out(context, "%s", prompt);
		line = fgets(buffer, len, context->cmdin);
	}

	if (line == NULL)
		return -1;

	if ((ptr = strchr(line, '\n')) != NULL)
		*ptr = '\0';

	if (use_readline) {
		strncpy(buffer, line, len);
		free(line);
	}
	if (!is_interactive(context))
		context_out(context, ">> %s\n", buffer);
	
	return 0;
}

int get_funct_params(struct app_context *context, char *line, 
		     struct funct_data *fdata, union param *pars)
{
	struct funct_param *fparam = fdata->params;
	union param *val = pars;
	int nparam, readinput, ret, i, optional, nomore;
	char buffer[MAX_STR_LEN], *ptr, *type;

	ptr = line;
	ptr += strspn(ptr, WHITE_SPC);   // skip white spaces ...
	ptr += strcspn(ptr, WHITE_SPC);  // ... and the command
	
	setstrlen(buffer, 0);
	for (nparam = 0; fparam->name; nparam++, fparam++, val++) {
		type = param_type_name[fparam->type & PARAM_TYPE_MASK];
		optional = fparam->type & PARAM_OPT;
		ptr += strspn(ptr, WHITE_SPC);

		nomore = (strlen(ptr) == 0);
		if (nomore && optional) {
			set_opt_param_not_present(val);
			continue;
		}
		readinput = (nomore && !optional);
		do {
			if (readinput) {
				context_out(context, "%s (%s)? ", 
					    fparam->desc, type);
				/* kirov: below it should be "context->cmdin" */
				if (!fgets(buffer, MAX_STR_LEN, context->in))
					return -1;
				i = strspn(buffer, WHITE_SPC);
				if (i > 0)
					memmove(buffer, buffer + i, 
						strlen(buffer) - i + 1);
				i = strcspn(buffer, WHITE_SPC);
				setstrlen(buffer, i);
			} else
				strcpy(buffer, ptr);

			ret = conv_param(buffer, fparam, val);
			if (ret == -1)
				context_err(context, "Invalid %s param. #%d: "
					    "%s\n", type, nparam + 1, buffer);
			else if (ret == -2)
				context_err(context, "Unbalanced quotes\n");
			if (ret < 0)
				continue;
			// ret has the number of bytes parsed
			if (!readinput) {
				ptr += ret;
				continue;
			}

			ret += strspn(buffer + ret, WHITE_SPC);
			if (strcspn(buffer + ret, WHITE_SPC) > 0)
				context_err(context, "Warning: ignoring "
					    "additional arguments: %s\n", 
					    buffer + ret); 
		} while (readinput && (ret < 0));

		if (ret == -1)
			return -1;
	}
	if (strcspn(ptr, WHITE_SPC) > 0)
		context_err(context, "Warning: ignoring additional "
			    "arguments: %s\n", ptr); 

	return nparam;
}


int is_cmd(char *input, char *command)
{
	unsigned len;
	input += strspn(input, WHITE_SPC);
	len = strcspn(input, WHITE_SPC);
	if (len != strlen(command))
		return 0;
	return (strncasecmp(input, command, len) == 0);
}

char *get_next_cmd(char *cmd)
{
	int i, quotes, in_quotes;
	char *end;

	in_quotes = 0;
	while (*cmd && (end = strchr(cmd, ';'))) {
		for (i = 0; cmd < end; i++, cmd++) {
			quotes = ((*cmd == '"') || (*cmd == '\''));
			if (i && (*(cmd-1) == '\\'))
				quotes = 0;
			if (!quotes)
				continue;
			if (!in_quotes) 
				in_quotes = *cmd;
			else if (in_quotes == *cmd)
				in_quotes = 0;
		}
		if (!in_quotes) {
			*cmd++ = 0;
			return cmd;
		}
		cmd = end + 1;
	}

	return NULL;
}

// ret: <0=Error, 0=OK, 1=End
int get_context_input(struct app_context **context_ptr)
{
	struct app_context *context = *context_ptr;
	struct input_data *input_data = context_input_data(context);
	struct funct_data *fdata; 
	union param *pars = input_data->pars;
	int ret, len, interactive = is_interactive(context);
	char *buffer, *cmd;

	buffer = context->cmd_buffer;
	len = sizeof(context->cmd_buffer);

	while (!context->app_end) {
		cmd = context->next_cmd;
		if (cmd == NULL) {
			ret = context_get_line(context, buffer, len);
			if (ret < 0)
				return 1;
			cmd = buffer;
		}

		context->next_cmd = get_next_cmd(cmd);

		cmd += strspn(cmd, " \t");
		if ((strlen(cmd) == 0) || (cmd[0] == '#'))
			continue;
				
		for (fdata = context->funcs; fdata->name; fdata++)
			if (is_cmd(cmd, fdata->name))
				break;
		if (!fdata->name) {
			for (fdata = framework_funcs; fdata->name; fdata++)
				if (is_cmd(cmd, fdata->name))
					break;
		}
		if (!fdata->name) {
			context_err(context, "Invalid command: %s\n", cmd);
			if (interactive) 
				continue;
			return -1;
		}
		
		if (is_cmd(fdata->name, "Abort")) {
			if (interactive) 
				continue;
			return 1;
		}

		context_out(context, "\n");

		ret = get_funct_params(context, cmd, fdata, pars);
		if (ret < 0) {
			if (interactive)
				continue;
			return -1;
		}
		
		input_data->fdata = fdata;
		return 0;
	}

	return 1;
}

void *input_thread_funct(void *data)
{
	struct app_context *context = (struct app_context *) data;
	struct input_data *input_data = context_input_data(context);
	int ret;

	do {
		ret = get_context_input(&context);

		pthread_mutex_lock(&input_data->signal_lock);
		input_data->ready = 1;
		input_data->ret = ret;
		pthread_cond_wait(&input_data->event, 
				  &input_data->signal_lock);
		pthread_mutex_unlock(&input_data->signal_lock);
	} while (ret != 1);

	return NULL;
}

int exec_function(struct app_context *context, struct input_data *input_data)
{
	struct funct_data *fdata = input_data->fdata; 
	union param *pars = input_data->pars;
	char *errstr;
	int ret;

	ret = fdata->funct(context, pars);
	if (context->err_funct)
		errstr = context->err_funct(context, fdata, ret);
	else
		errstr = (ret < 0) ? "Application error" : NULL;
	if (errstr) {
		context_err(context, "Error in %s: %s\n", fdata->name, errstr);
		context_out(context, "\n");
		return is_interactive(context) ? 0 : -1;
	} else if (ret > 0)
		context_out(context, "Function %s returned %d "
			    "(0x%x)\n", fdata->name, ret, ret);
	
	context_out(context, "\n");

	return 0;
}

int exec_application(struct app_context *context)
{
	int ret, i, input_thread;
	struct input_data input_data;
	poll_funct *funct;

	input_data.ready = 0;
	context->input_data = &input_data;
	context->app_end = 0;

	input_thread = (context->poll_funcs != NULL);
	if (input_thread) {
		pthread_cond_init(&input_data.event, NULL);
		pthread_mutex_init(&input_data.signal_lock, NULL);

		ret = pthread_create(&input_data.thread, NULL, 
				     input_thread_funct, context);
		if (ret != 0)
			return ret;
	}

	for (;;) {
		if (!input_thread) {
			ret = get_context_input(&context);
			if (ret == 0)
				ret = exec_function(context, &input_data);
			if (ret != 0)
				break;
			continue;
		}

		ret = 0;
		pthread_mutex_lock(&input_data.signal_lock);
		if (input_data.ready) {
			ret = input_data.ret;
			if (ret == 0)
				ret = exec_function(context, &input_data);
			input_data.ready = 0;
			pthread_cond_signal(&input_data.event);
		}
		pthread_mutex_unlock(&input_data.signal_lock);
		if (ret != 0)
			break;

		for (i = 0; i < context->nr_poll_funcs; i++) {
			funct = context->poll_funcs[i];
			funct(context);
		}

		exact_usleep(context->poll_sleep_usec);
	}

	if (input_thread) {
		pthread_cancel(input_data.thread);
		pthread_join(input_data.thread, NULL);
		context->input_data = NULL;
	}

	context_out(context, "\n");

	return (ret < 0) ? ret : 0;
}

void print_funct_help(struct app_context *context, struct funct_data *fdata,
		      int long_desc)
{
	struct funct_param *fparam;
	int i, opt, npars;
	char *type;

	i = context_out(context, "  %-*s", 15, fdata->name);
	npars = 0;
	for (fparam = fdata->params; fparam->name; fparam++, npars++) {
		opt = fparam->type & PARAM_OPT;
		i += context_out(context, " %s%s%s", opt?"[":"", fparam->name,
						     opt?"]":"");
	}
	if ((i >= 40) || long_desc) {
		context_out(context, "\n");
		i = 0;
	}
        if (long_desc)
		i += context_out(context, "    Description:");
	context_out(context, "%*s%s\n", 40 - i, "", fdata->desc);

	if (!long_desc || (npars == 0))
		return;

	context_out(context, "    Parameters:\n");
	for (fparam = fdata->params; fparam->name; fparam++, npars++) {
		i = context_out(context, "      %s", fparam->name);
		type = param_type_name[fparam->type & PARAM_TYPE_MASK];
		i += context_out(context, "%*s(%s)", 25 - i, "", type);
		context_out(context, "%*s%s\n", 40 - i, "", fparam->desc);
	}
}

int print_help(struct app_context *context, union param *pars)
{
	struct funct_data *fdata, *funcs[] = {context->funcs, framework_funcs};
	unsigned long_desc, i, cont;
	char *cmd;

	long_desc = chk_opt_param_present(&pars[0]);
	cmd = get_param_str(&pars[0]);

	cont = 1;
	for (i = 0; (i < sizeof(funcs) / sizeof(funcs[0])) && cont; i++) {
		for (fdata = funcs[i]; fdata->name && cont; fdata++) {
			if (!long_desc || (strcasecmp(fdata->name, cmd) == 0)){
				print_funct_help(context, fdata, long_desc);
				if (long_desc)
					cont = 0;
			}
		}
	}

	if (long_desc && cont)
		context_out(context, "Command \"%s\" not found\n", cmd);

	return 0;
}


int run_script(struct app_context *context, union param *pars)
{
	struct app_context *new_context;
	char *file_name;
	FILE *file;

	if( (NULL == pars) || (NULL == (file_name=get_param_str(&pars[0]))) ) {
		context_out(context, "Bad or missing file-name\n");
		return -1;
	}

	file = fopen(file_name, "r");
	if( NULL == file ) {
		context_out(context, "Error opening file \"%s\"\n", file_name);
		return -1;
	}

	context_out(context, "Beware! This functionality is experimental!\n");

	new_context=(struct app_context *)calloc(1, sizeof(struct app_context));
	if( NULL == new_context ) {
		context_out(context, "Error allocating memory\n");
		return -1;
	}

	/* Lock */
	/* Shallow copy current context; Is this enough??? */
	memcpy( new_context, context, sizeof(struct app_context) );
	/* Unlock */

	new_context->in = file;
	new_context->cmdin = file;
	new_context->interactive = 0;
        new_context->name = file_name; /* ??? */
	new_context->input_name	= file_name; /* ??? */
	/* What else??? */

	exec_application( new_context );

	/* Lock */
	/* Take the changes: context <-- new_context ??? */
	context->app_end = new_context->app_end;
	/* Unlock */

	free( new_context );

	fclose( file );

	return 0;
}


void exact_usleep(long usec)
{
	struct timeval now, last;

	if (usec > 0) {
		if (usec >= DEFAULT_SYSTEM_TICK) {
			usleep(usec);
		} else {
			gettimeofday(&last, NULL);
			do {
				gettimeofday(&now, NULL);
			} while (elapsed_usec(&last, &now) < usec);
		}
	}
}

int do_sleep(struct app_context *context, union param *pars)
{
	float sec = pars[0].f;
	long usec = (long) (sec * 1e6);

	if (sec < 0)
		return -EINVAL;
	else if (sec == 0)
		return 0;

	exact_usleep(usec);
	return 0;
}

int quit_app(struct app_context *context, union param *pars)
{
	context->app_end = 1;
	return 0;
}

int dump_data(struct app_context *context, void *data, int len)
{
	int offset, i, chrs, linechrs = 16;
	unsigned char *ptr = (unsigned char *) data;

#define isprint7(i)	(((i) < 0x80) && isprint(i))

	for (offset = 0; offset < len; offset += linechrs) {
		chrs = linechrs;
		if (offset + chrs > len)
			chrs = len - offset;
		context_out(context, "%04x:  ", offset);
		for (i = 0; i < chrs; i++)
			context_out(context, "%02x ", ptr[i]);
		for (i = chrs; i < linechrs; i++)
			context_out(context, "   ");
		context_out(context, " ");
		for (i = 0; i < chrs; i++)
			context_out(context, "%c", 
				    isprint7(ptr[i]) ? ptr[i] : '.');
		context_out(context, "\n");
		ptr += chrs;
	}

	return 0;
}

int system_shell(struct app_context *context, union param *pars)
{
	return system(get_param_str(&pars[0]));
}
