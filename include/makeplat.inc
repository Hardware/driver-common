#
#  Makefile include for platform definition for ESRF drivers. 
#  The copybin functionality separates binaries depending on the
#  compiling platform (OS, esrflinux ARCH, TARGET_KERN). It is
#  activated by default inside the ESRF and deactivated outside.
#
#  This file must be included first, and maketarget.inc must be
#  included at the end.
#
#  $Header: /segfs/bliss/cvs/driver/ESRF/common/include/makeplat.inc,v 1.18 2017/12/18 13:59:37 ahoms Exp $
#

CC		?= gcc
CXX		?= g++

ENV_MAKE_INC	= $(shell echo $$ESRF_DRV_MAKE_INC)
ifneq ($(ENV_MAKE_INC),)
include $(ENV_MAKE_INC)
endif

ifndef TARGET_KERN
TARGET_KERN	:= $(shell uname -r)
endif

LINUX_PATCH	= $(shell echo $(1) | cut -d. -f1-2)
LINUX_MAJOR	= $(shell echo $(1) | cut -d. -f1)
LINUX_MINOR	= $(shell echo $(1) | cut -d. -f2)
LINUX_RELEASE	= $(shell echo $(1) | cut -d. -f3 | cut -d- -f1)

LINUX_OLDER_THAN = $(shell test \
		$(call LINUX_MAJOR,$(1)) -lt $(call LINUX_MAJOR,$(2)) -o \
		$(call LINUX_MAJOR,$(1)) -eq $(call LINUX_MAJOR,$(2)) -a \
		$(call LINUX_MINOR,$(1)) -lt $(call LINUX_MINOR,$(2)) -o \
		$(call LINUX_MAJOR,$(1)) -eq $(call LINUX_MAJOR,$(2)) -a \
		$(call LINUX_MINOR,$(1)) -eq $(call LINUX_MINOR,$(2)) -a \
		$(call LINUX_RELEASE,$(1)) -lt $(call LINUX_RELEASE,$(2)) \
					&& echo YES || echo NO)

TARGET_KERN_PATCH := $(call LINUX_PATCH,$(TARGET_KERN))


TARGET_KERN_MINOR := $(shell echo $(TARGET_KERN) | cut -d. -f2)

ifndef KERNEL_SOURCES
KERNEL_SOURCES	= /lib/modules/$(TARGET_KERN)/build
endif

ifndef TARGET_KERN_USES_CFLAGS
TARGET_KERN_USES_CFLAGS = $(call LINUX_OLDER_THAN,$(TARGET_KERN),2.6.24)
ifeq ($(TARGET_KERN_USES_CFLAGS), YES)
TARGET_KERN_USES_EXTRA_CFLAGS = NO
TARGET_KERN_USES_CCFLAGS_Y = NO
endif
endif

ifndef TARGET_KERN_USES_EXTRA_CFLAGS
TARGET_KERN_USES_EXTRA_CFLAGS = $(call LINUX_OLDER_THAN,$(TARGET_KERN),3.2.0)
ifeq ($(TARGET_KERN_USES_EXTRA_CFLAGS), YES)
TARGET_KERN_USES_CCFLAGS_Y = NO
endif
endif

ifndef TARGET_KERN_USES_CCFLAGS_Y
TARGET_KERN_USES_CCFLAGS_Y = YES
endif

INSIDE_ESRF	:= $(shell test -d /segfs/bliss && \
                           test -x /csadmin/common/scripts/get_esrflinux && \
			   echo YES)

ifeq ($(INSIDE_ESRF), YES)
GET_ESRFLINUX	= /csadmin/common/scripts/get_esrflinux
ESRFLINUX_STATE	= $(shell $(GET_ESRFLINUX) > /dev/null 2>&1 && echo OK)
GET_OS		= /csadmin/common/scripts/get_os.share

ifndef COPYBIN
COPYBIN		= YES
endif

ifndef COPY2DEPOT
COPY2DEPOT	= YES
endif

ifndef OS
OS		:= $(shell $(GET_OS))
endif 

ifndef ARCH
ifeq ($(ESRFLINUX_STATE), OK)
ARCH		:= $(shell $(GET_ESRFLINUX) --architecture)
else
ARCH		:= $(shell uname -m)
endif
endif  # ARCH

endif  # INSIDE_ESRF

ifndef COPYBIN
COPYBIN		= YES
endif

ifndef COPY2DEPOT
COPY2DEPOT	= NO
endif

ifndef OS
OS		:= $(shell uname)
endif

ifndef ARCH
ARCH		:= $(shell uname -i)
ifeq ($(ARCH), unknown)
ARCH		:= $(shell uname -m)
endif
ifeq ($(ARCH), unknown)
ARCH		:= x86
endif
endif  # ARCH

TARGETOS	= $(OS)

ifeq ($(COPYBIN), YES)
# The modules directory
ifndef MOD_DIR 
ifndef MOD_BASE
MOD_BASE	= modules
endif

MOD_DIR		= $(MOD_BASE)/$(ARCH)/$(TARGET_KERN)
endif # MOD_DIR

# The bin directory
ifndef BIN_DIR
BIN_DIR		= bin/$(ARCH)
endif

# The lib directory
ifndef LIB_DIR
LIB_DIR		= lib/$(ARCH)
endif

MAKE_COPYBIN	= copybin
CLEAN_COPYBIN	= cleanbin
endif # COPYBIN

ifeq ($(COPY2DEPOT), YES)
BLISSDEPOT_BASE	= /segfs/bliss/source/driver
ifndef DEPOT_BASE
DEPOT_BASE	= $(BLISSDEPOT_BASE)
endif

ifndef DEPOT_MOD_DIR
DEPOT_MOD_DIR	= $(DEPOT_BASE)/modules/$(OS)/$(ARCH)/$(TARGET_KERN)
endif
ifndef DEPOT_BIN_DIR
DEPOT_BIN_DIR	= $(DEPOT_BASE)/bin/$(OS)/$(ARCH)
endif
ifndef DEPOT_LIB_DIR
DEPOT_LIB_DIR	= $(DEPOT_BASE)/lib/$(OS)/$(ARCH)
endif

DEPOT_TARGET	= real_copy2depot
endif # COPY2DEPOT

MAKE_OPTS	:= TARGET_KERN=$(TARGET_KERN) 


