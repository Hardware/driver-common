/****************************************************************************
 * File:	circ_buffer.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/common/include/circ_buffer.h,v 1.7 2007/10/23 15:07:30 ahoms Exp $
 * Project:	SECAD Espia card Linux driver
 * Description:	Circular buffer header file
 * Author(s):	A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 ****************************************************************************/

#ifndef _CIRC_BUFFER_H
#define _CIRC_BUFFER_H

#include <linux/spinlock.h>
#include <linux/sched.h>
#include <asm/div64.h>

#include "esrfversion.h"

#define CIRC_BUFFER_READ		0x00
#define CIRC_BUFFER_WRITE		0x01
#define CIRC_BUFFER_KERNEL		0x00
#define CIRC_BUFFER_USER		0x02
#define CIRC_BUFFER_KTHREAD		0x04
#define CIRC_BUFFER_NO_BLOCK		0
#define CIRC_BUFFER_BLOCK_FOREVER	((unsigned long) -1)

#define CIRC_BUFFER_DEB_NONE		0
#define CIRC_BUFFER_DEB_ERRORS		1	     
#define CIRC_BUFFER_DEB_PARAMS		2
#define CIRC_BUFFER_DEB_XFER		3

extern int circ_buffer_debug_level;


struct circ_buffer_agent {
	volatile unsigned long	 	 ptr;
	volatile unsigned long	 	 wait_count;
	wait_queue_head_t	 	 wq;
};

struct circ_buffer_xfer_data {
	char			 	*data;
	unsigned long		 	 data_len;
	int			 	 flags;
	char			 	*term;
	unsigned long		 	 term_len;
	unsigned long			 term_found_bytes;
	int			 	 timedout;
};

struct circ_buffer {
	char			 	*buffer;
	unsigned long		 	 len;
	struct circ_buffer_agent	 read;
	struct circ_buffer_agent	 write;
	struct circ_buffer_xfer_data	*xfer;
};

int circ_buffer_init_agent(struct circ_buffer_agent *agent);
int circ_buffer_init   (struct circ_buffer *buff, unsigned long len);
int circ_buffer_cleanup(struct circ_buffer *buff);
int circ_buffer_bytes  (struct circ_buffer *buff, int free, spinlock_t *lock);
int circ_buffer_check_block(struct circ_buffer *buff, 
			    struct circ_buffer_xfer_data *xfer, 
			    spinlock_t *lock, unsigned long timeout_us);
int circ_buffer_copy_data(struct circ_buffer *buff, 
			  struct circ_buffer_xfer_data *xfer);
int circ_buffer_xfer   (struct circ_buffer *buff, 
			struct circ_buffer_xfer_data *xfer, 
			spinlock_t *lock, unsigned long timeout_us);
int circ_buffer_update_ptr(struct circ_buffer *buff, 
			   struct circ_buffer_xfer_data *xfer, spinlock_t *lock);
int circ_buffer_putc   (struct circ_buffer *buff, int chr, spinlock_t *lock, 
			int flags, unsigned long timeout_us);
int circ_buffer_getc   (struct circ_buffer *buff, spinlock_t *lock, 
			int flags, unsigned long timeout_us);
int circ_buffer_flush  (struct circ_buffer *buff, spinlock_t *lock);

char *get_char_repr(char ch);

#define circ_buffer_flag(xfer, flag) \
	(((xfer)->flags & (flag)) != 0)


static inline
void circ_buffer_xfer_data_init(struct circ_buffer_xfer_data *xfer,
				char *data, unsigned long data_len,
				char *term, unsigned long term_len, int flags)
{
	xfer->data     = data;
	xfer->data_len = data_len;
	xfer->term     = term;
	xfer->term_len = term_len;
	xfer->flags    = flags;
	xfer->timedout = xfer->term_found_bytes = 0;
}
				
static inline unsigned long circ_buffer_timeout(unsigned long timeout)
{
	long long aux;

	if (timeout == CIRC_BUFFER_BLOCK_FOREVER)
		return MAX_SCHEDULE_TIMEOUT;
	aux = (long long) timeout * HZ;
	do_div(aux, 1000000);
	return aux + 1;
}


#endif /* _CIRC_BUFFER_H */
