/***********************************************************************
 *  File:       esrfdebug.h
 *  $Header: /segfs/bliss/cvs/driver/ESRF/common/include/esrfdebug.h,v 1.9 2017/11/15 12:04:22 ahoms Exp $
 *  Project:    ESRF device driver interface for Linux
 *  Desc:       Debug helper functions and macros
 *  Author(s):  A. Homs <ahoms@esrf.fr>
 ***********************************************************************/

#ifndef _ESRFDEBUG_H
#define _ESRFDEBUG_H

#ifndef __KERNEL__
#include <stdio.h>		// for NULL
#include <string.h>		// for strlen
#endif

#define DEBUG_LEVEL	(*__deb_level)

#define __DECLARE_DEBUG_LEVEL(deb_lvl_name) \
static int *__deb_level = &(deb_lvl_name)

#define DECLARE_GLOBAL_DEBUG_LEVEL(deb_lvl_name, init_deb_lvl) \
int deb_lvl_name = (init_deb_lvl) 

#define DECLARE_DEBUG_LEVEL(deb_lvl_name, init_deb_lvl) \
DECLARE_GLOBAL_DEBUG_LEVEL(deb_lvl_name, init_deb_lvl); \
__DECLARE_DEBUG_LEVEL(deb_lvl_name)

#define DECLARE_STATIC_DEBUG_LEVEL(deb_lvl_name, init_deb_lvl) \
static DECLARE_DEBUG_LEVEL(deb_lvl_name, init_deb_lvl)

#define EXTERN_DEBUG_LEVEL(deb_lvl_name) \
extern int deb_lvl_name; \
__DECLARE_DEBUG_LEVEL(deb_lvl_name)


#define DEB(lvl, x)	do { if (DEBUG_LEVEL >= (lvl)) (x); } while (0)

#define DEB_FNAME	__deb_fname
#define FENTRY(name)	char *DEB_FNAME = name

#define DEB_FMT		DEB_HEADING ": %s: "
#define DEB_ARG		DEB_FNAME

#ifdef __KERNEL__

/* Warning! Space necessary between DEB_ARG and the comma */
#define DPRINTK(lvl, fmt, ...) \
	DEB((lvl), printk(DEB_FMT fmt, DEB_ARG , ## __VA_ARGS__))

#else /* ! __KERNEL__ */

#ifndef DEB_FILE
#define DEB_FILE	stderr
#endif
#define DPRINTF(lvl, fmt, ...) \
	DEB((lvl), fprintf(DEB_FILE, DEB_FMT fmt, DEB_ARG , ## __VA_ARGS__))

#endif /* __KERNEL__ */

typedef struct {
	long val;
	char *str;
} deb_const_str;

typedef struct {
	deb_const_str *array;
	int dim;
	char *prefix;
} deb_const_str_array;

static inline 
char *get_deb_const_str(deb_const_str_array *conststr, long val, char *notfound)
{
	int idx;
	for (idx = 0; idx < conststr->dim; idx++)
		if (conststr->array[idx].val == val)
			return conststr->array[idx].str;

	return notfound;
}

#define CONST_NAME(x)		{x, #x}

#define INIT_CONST_STR_ARRAY_PREFIX(a, b, p) \
	deb_const_str_array a = {b, sizeof(b) / sizeof(deb_const_str), p}

#define INIT_CONST_STR_ARRAY(a, b) \
	INIT_CONST_STR_ARRAY_PREFIX(a, b, NULL)

#define INIT_STATIC_CONST_STR_ARRAY_PREFIX(a, b, p) \
	static INIT_CONST_STR_ARRAY_PREFIX(a, b, p)

#define INIT_STATIC_CONST_STR_ARRAY(a, b) \
	static INIT_CONST_STR_ARRAY_PREFIX(a, b, NULL)

#define GET_CONST_STR(a, v, s)	get_deb_const_str(&(a), v, s)

#define PRETTY_RCS_REV_LEN	32

static inline 
char *pretty_rcs_revision(char *buffer, char *rev)
{
	char *aux, *rp = rev, *wp = buffer;
	char *name[2][2] = {
		{"Name: ", "Release "}, {"Revision: ", "Revision "}
	};
	int len, i, nr_name = sizeof(name) / sizeof(name[0]);
	
	while (*rp && (*rp == ' '))
		rp++;
	if (*rp++ != '$')
		return rev;

	for (i = 0; i < nr_name; i++) {
		aux = name[i][0];
		len = strlen(aux);
		if (strncmp(rp, aux, len) == 0) {
			wp += strlen(strcpy(wp, name[i][1]));
			rp += len;
			break;
		}
	}
	if (i == nr_name)
		return rev;

	aux = strchr(rp, '$');
	if (aux == NULL)
		return rev;

	while ((aux > rp) && aux[-1] == ' ')
		aux--;
	if (aux == rp)
		wp += sprintf(wp, "[Devel]");
	else
		for (; rp < aux; rp++)
			*wp++ = (*rp != '_') ? *rp : '.';
	*wp = 0;
	return buffer;
}

static inline 
char *numeric_rcs_revision(char *buffer, char *rel, char *rev)
{
	int i;

	if (pretty_rcs_revision(buffer, rel) == NULL)
		return NULL;

	if (strcmp(buffer, "Release [Devel]") != 0)
		return buffer;

	i = sprintf(buffer, "Devel. ");
	if (pretty_rcs_revision(buffer + i, rev) == NULL)
		return NULL;

	return buffer;
}


#endif /* _ESRFDEBUG_H */
