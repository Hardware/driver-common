# $Header: /segfs/bliss/cvs/driver/ESRF/common/include/makercstarget.inc,v 1.2 2005/03/23 14:48:10 ahoms Rel $

# The Makefile must define the variable:
#	RCSFILES:   expanded list of all RCS files
# with the syntax:
#	file01 file02 ... [ dir1/ file11 file12 ... ] [dir2/ file21 ... ] ...
# The list will be applied to the merge script, which will expand it in
# a list of file names with the corresponding path. The merge script must
# be defined (in makercs.inc): $(MERGESCRIPTDIR)/$(MERGESCRIPTNAME)

ifdef DOSFILES
U2DALL		= $(UNIX2DOSALL)
DIFFOPT		= -b
endif

RCSFULLNAMES	= $(shell $(MERGESCRIPTDIR)/$(MERGESCRIPTNAME) $(RCSFILES)) 

lock:
	$(RCSLOCK) 	$(RCSFULLNAMES)
	$(U2DALL)
unlock:
	$(RCSUNLOCK) 	$(RCSFULLNAMES)
	$(U2DALL)
co:
	$(RCSCO) 	$(RCSFULLNAMES)
	$(U2DALL)
ci:
	$(RCSCI) 	$(RCSFULLNAMES)
	$(U2DALL)
diff:
	$(RCSDIFF)	$(RCSFULLNAMES)
tkdiff:
	for f in $(RCSFULLNAMES); do \
		if ! $(RCSDIFF) $$f > /dev/null 2>&1; then \
			$(TKDIFF) $$f &	\
		fi \
	done
rcsnames:
	@echo	 	$(RCSFULLNAMES)

