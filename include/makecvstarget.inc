#
#  Makefile include for common CVS target definitions for ESRF drivers.
#
#  $Header: /segfs/bliss/cvs/driver/ESRF/common/include/makecvstarget.inc,v 1.4 2008/07/28 15:19:26 ahoms Exp $
#

ifeq ($(COMMON_DIR),)
ifneq ($(COMMON_INCL),)
COMMON_DIR	= $(COMMON_INCL)/..
else
COMMON_DIR	= ../common
endif
endif

COMMON_BIN	= $(COMMON_DIR)/bin

ifeq ($(CVS_REV),)
ifneq ($(VERSION),)
CVS_REV		= -r $(VERSION)
endif
endif

tkdiff:
	@for f in `$(MAKE) cvsnames | grep -v '^make\[[0-9]\+\]'`; do \
		if ! cvs diff $(CVS_REV) $$f > /dev/null; then \
			tkdiff $(CVS_REV) $$f & \
		fi \
	done

cvsnames:
	@find . -name Entries | grep 'CVS/Entries' | while read f; do \
		dir=`dirname $$f`; \
		dir=`dirname $$dir`; \
		grep '^/' $$f | cut -d/ -f2 | awk "{print \"$$dir/\" \$$1}"; \
	done

cvstags:
	@$(COMMON_BIN)/cvstags
