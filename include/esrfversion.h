/***********************************************************************
 *  File:       esrfversion.h
 *  $Header: /segfs/bliss/cvs/driver/ESRF/common/include/esrfversion.h,v 1.21 2017/12/18 13:59:37 ahoms Exp $
 *  Project:    ESRF device driver interface for Linux
 *  Desc:       Version-dependent definitions
 *  Author(s):  A. Homs <ahoms@esrf.fr>
 ***********************************************************************/

#ifndef _ESRFVERSION_H
#define _ESRFVERSION_H


/*------------------------------------------------------------------
 * Kernel version
 *------------------------------------------------------------------*/

#include <linux/version.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,4,0)
#error "Not tested with kernels < 2.4"
#elif LINUX_VERSION_CODE >= KERNEL_VERSION(6,9,0)
#error "Not tested with kernels >= 6.9.0 " LINUX_VERSION_CODE
#endif

#if LINUX_VERSION_CODE == KERNEL_VERSION(2,6,9)
#ifdef __GFP_WIRED
#define ESRFVER_ESRFLINUX_1_1
#else
#define ESRFVER_ESRFLINUX_1_2
#endif
#endif


/*------------------------------------------------------------------
 *                  2.4 / 2.6 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * Module parameters and symbol table
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define	MODULE_PARM_STR_byte	"i"
#define	MODULE_PARM_STR_short	"h"
#define	MODULE_PARM_STR_ushort	"h"
#define	MODULE_PARM_STR_int	"i"
#define	MODULE_PARM_STR_uint	"i"
#define	MODULE_PARM_STR_long	"l"
#define	MODULE_PARM_STR_ulong	"l"

#define module_param(var, type, access)		\
	MODULE_PARM(var, MODULE_PARM_STR_##type)

#define module_param_string(name, string, len, perm) 			\
	MODULE_PARM(var, "s")

#define EXPORT_SYMTAB

#endif // Linux < 2.6


/*------------------------------------------------------------------
 * Sleep while condition (wait_event) with timeout implementation
 *------------------------------------------------------------------*/

#include <linux/sched.h>
#include <linux/wait.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

struct wait_event_timeout_stc {
	task_t		*proc;
	volatile int	timeout;
};

static __attribute__((unused))
void wait_event_timeout(unsigned long data)
{
	struct wait_event_timeout_stc *event_stc;
	event_stc = (struct wait_event_timeout_stc *) data;
	event_stc->timeout = 1;
	wake_up_process(event_stc->proc);
}


/* wait_event_interruptible_timeout:
	1		condition met or waken up normally
        0		if there was a timeout
	-ERESTARTSYS	if received a signal
*/

#define wait_event_interruptible_timeout(wq, condition, tout)		\
({									\
	struct timer_list event_timer;					\
	struct wait_event_timeout_stc event_stc;			\
	int __ret = 0;							\
	unsigned long timeout = (tout);					\
									\
	event_stc.timeout = 0;						\
									\
	if (timeout != MAX_SCHEDULE_TIMEOUT) {				\
		event_stc.proc = current;				\
									\
		init_timer(&event_timer);				\
		event_timer.expires = jiffies + timeout;		\
		event_timer.data = (unsigned long) &event_stc;		\
		event_timer.function = wait_event_timeout;		\
		add_timer(&event_timer);				\
	}								\
									\
	__ret = wait_event_interruptible(wq, (condition) ||		\
					     event_stc.timeout);	\
									\
	if (timeout != MAX_SCHEDULE_TIMEOUT) {				\
		del_timer(&event_timer);				\
	}								\
									\
	if (__ret == 0)							\
		__ret = !event_stc.timeout;				\
	__ret;								\
})

#endif // Linux < 2.6


/*------------------------------------------------------------------
 * PCI devices
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define pci_name(dev)			((dev)->name)

#define ESRFVER_NO_PCI_CONSISTENT_DMA	1

#endif // Linux < 2.6


/*--------------------------------------------------------------------------
 * msleep
 *--------------------------------------------------------------------------*/

#include <linux/interrupt.h>
#include <linux/delay.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

static inline
int msleep(unsigned long ms)
{
	int ret;

	if (in_interrupt()) {
		printk("msleep: Warning: in interrupt!\n");
		udelay(ms * 1000);
		ret = -1;
	} else {
		unsigned long ticks = (ms * HZ) / 1000 + 1;
		DECLARE_WAIT_QUEUE_HEAD(wq);
		ret = interruptible_sleep_on_timeout(&wq, ticks);
	}

	return ret;
}

#endif // Linux < 2.6


/*------------------------------------------------------------------
 * IRQ handling
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define KM_SOFTIRQ0			KM_IRQ0
#define KM_SOFTIRQ1			KM_IRQ1

typedef void				irqreturn_t;
#define DECLARE_IRQ_RET(ret, val)	do {} while (0)
#define SET_IRQ_RET(ret, val)		do {} while (0)
#define RETURN_IRQ(ret)			do {} while (0)

#else  // Linux >= 2.6

#define DECLARE_IRQ_RET(ret, val)	irqreturn_t ret = (val)
#define SET_IRQ_RET(ret, val)		ret = (val)
#define RETURN_IRQ(ret)			return ret

#endif // Linux >= 2.6


/*------------------------------------------------------------------
 * Virtual memory and page map
 *------------------------------------------------------------------*/

#include <linux/pagemap.h>

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define ESRFVER_REMAP_PAGE_FORCE

#define alloc_page_vma(gfp, vma, addr)	alloc_page(gfp)
#define page_to_pfn(page)		PFN_DOWN(page_to_phys(page))

#endif // Linux < 2.6.0


#ifdef ESRFVER_REMAP_PAGE_FORCE

static __attribute__((unused))
int map_single_page(struct vm_area_struct *vma, unsigned long addr, 
		    struct page *page, pgprot_t pgprot)
{
	struct mm_struct *mm = vma->vm_mm;
	unsigned long unmapped = 0;
	pmd_t *pmd;
	pte_t *pte, old_pte;
	pgd_t *pgd;

	pgd = pgd_offset(mm, addr);
	if (!pgd_none(*pgd)) {
		pmd = pmd_offset(pgd, addr);
		if (!pmd_none(*pmd)) {
			pte = pte_offset_atomic(pmd, addr);
			old_pte = ptep_get_and_clear(pte);
			unmapped = !pte_none(old_pte);
			if (page)
				set_pte(pte, mk_pte(page, pgprot));
			pte_kunmap(pte);
		}
	}
	return unmapped;
}

static __attribute__((unused))
int unmap_page_range(struct vm_area_struct *vma, unsigned long addr,
                     unsigned long size)
{
	unsigned long pos, end, unmapped, page_unmap;

	unmapped = 0;
	end = addr + PAGE_ALIGN(size);
	flush_cache_range(mm, addr, end);
	for (pos = addr; pos < end; pos += PAGE_SIZE) {
		page_unmap = map_single_page(vma, pos, NULL, __pgprot(0));
		if (page_unmap)
			unmapped = 1;
	}

	return unmapped;
}

#endif


/*------------------------------------------------------------------
 * Kernel threads
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define ESRFVER_KTHREAD_V1

struct kthread_data {
	int			      (*fn)(void *data);
	void			       *data;
	struct task_struct	       *task;
	volatile int			running;
	volatile int			finished;
	volatile int			ret;
	wait_queue_head_t		wq;
};

typedef struct kthread_data	       *kthread_t;
#define kthread_task(th)		((th)->task)

#else  // Linux >= 2.6

#include <linux/kthread.h>

typedef struct task_struct	       *kthread_t;
#define kthread_task(th)		(th)

#endif // Linux >= 2.6


#ifdef ESRFVER_KTHREAD_V1

#define PF_KTHREAD_STOP		(1UL<<30)

#include <asm/uaccess.h>
#include <linux/unistd.h>

static inline _syscall3(int,setresuid32,uid_t,ruid,uid_t,euid,uid_t,suid);
static inline _syscall3(int,setresgid32,gid_t,rgid,gid_t,egid,gid_t,sgid);
static inline _syscall1(int,setfsuid32,uid_t,uid);
static inline _syscall1(int,setfsgid32,gid_t,gid);

static inline 
int kthread_should_stop()
{
	int stop;

        task_lock(current);
	stop = current->flags & PF_KTHREAD_STOP;
        task_unlock(current);

	return stop;
}

static __attribute__((unused)) 
int kthread_stop(kthread_t th)
{
	struct task_struct *task = kthread_task(th);
	int ret;

        task_lock(task);
	task->flags |= PF_KTHREAD_STOP;
        task_unlock(task);

	wake_up_process(kthread_task(th));
	wait_event(th->wq, th->finished);
	ret = th->ret;
	th->finished = 2;
	wake_up(&th->wq);
	return ret;
}

struct kthread_aux {
	wait_queue_head_t	 wq;
	volatile kthread_t	 th;
	volatile int		 th_valid;
};

static __attribute__((unused)) 
int kthread_main_fn(void *data)
{
	struct kthread_aux *th_aux = (struct kthread_aux *) data;
	struct kthread_data th_data;
	kthread_t th = &th_data;

	set_fs(KERNEL_DS);
	current->cap_effective = current->cap_permitted = CAP_INIT_EFF_SET;
	setresuid32(0, 0, 0);
	setresgid32(0, 0, 0);
	setfsuid32(0);
	setfsgid32(0);
	daemonize();

	init_waitqueue_head(&th->wq);
	th->running = th->finished = 0;
	th->task = current;
	th_aux->th = th;
	th_aux->th_valid = 1;
	wake_up(&th_aux->wq);

	set_current_state(TASK_INTERRUPTIBLE);
	th->ret = schedule_timeout(MAX_SCHEDULE_TIMEOUT);
	if (th->ret < 0)
		goto out;
	else if (kthread_should_stop(th)) {
		th->ret = -EINTR;
		goto out;
	}
	
	th->running = 1;
	th->ret = th->fn(th->data);
 out:
	th->finished = 1;
	wake_up(&th->wq);
	wait_event(th->wq, th->finished == 2);
	return th->ret;
}

#define kthread_create(threadfn, threaddata, namefmt, ...) \
({							   \
	struct kthread_aux th_aux;			   \
	int pid;					   \
	kthread_t th;					   \
	init_waitqueue_head(&th_aux.wq);		   \
	th_aux.th_valid = 0;				   \
	pid = kernel_thread(kthread_main_fn, &th_aux, 0);  \
	if (pid < 0) {					   \
		th = (kthread_t) ERR_PTR(pid);		   \
		goto kthread_run_ret;			   \
	}						   \
	wait_event(th_aux.wq, th_aux.th_valid);		   \
	th = th_aux.th;					   \
	th->fn   = (threadfn);				   \
	th->data = (threaddata);			   \
	sprintf(th->task->comm, namefmt, ## __VA_ARGS__);  \
							   \
 kthread_run_ret:					   \
	th;						   \
})

#define kthread_run(threadfn, data, namefmt, ...)			\
({									\
        struct task_struct *task;					\
        kthread_t th;							\
        th = kthread_create(threadfn, data, namefmt, ## __VA_ARGS__);	\
        if (!IS_ERR(th)) {						\
		task = kthread_task(th);				\
                wake_up_process(task);					\
	}								\
	th;								\
})

#endif // ESRFVER_KTHREAD_V1


/*------------------------------------------------------------------
 * Class device support
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)
#define ESRFVER_NO_CLASS_DEV
#endif 


/*------------------------------------------------------------------
 *                  pre-2.6.10/11 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * Virtual memory and page map
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10)
#include <asm/setup.h>

#define ESRFVER_REMAP_PAGE_RESERVED

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#define remap_pfn_range(vma, vaddr, pfn, size, pgprot) \
	remap_page_range(vaddr, PFN_PHYS(pfn), size, pgprot)

#else  // Linux >= 2.6.0

#define ESRFVER_REMAP_PAGE_32

#define remap_pfn_range(vma, vaddr, pfn, size, pgprot) \
	remap_page_range(vma, vaddr, PFN_PHYS(pfn), size, pgprot)

#endif // Linux >= 2.6.0

#endif // Linux < 2.6.10

/*------------------------------------------------------------------
 * set scheduler type
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,10)
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,0)

#include <linux/syscall.h>
#define sched_setscheduler(task, policy, sched_param) \
	sys_sched_setscheduler((task)->pid, policy, sched_param)

#else  // Linux >= 2.6.0

#include <linux/unistd.h>
#define __NR_aux_sched_setscheduler	__NR_sched_setscheduler 
static inline _syscall3(int,aux_sched_setscheduler,pid_t,pid,int,policy,
			struct sched_param *,param);
#define sched_setscheduler(task, policy, sched_param) \
	aux_sched_setscheduler((task)->pid, policy, sched_param)
#define ESRFVER_SCHED_SET_SCHED_SYSCALL

#endif // Linux < 2.6.0
#endif // Linux < 2.6.10


/*------------------------------------------------------------------
 *                  pre-2.6.12/13 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * New class device structure
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,12)
#define ESRFVER_CLASS_DEV_NO_DEVT
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,13)
#define ESRFVER_CLASS_DEV_NO_OWNER
#endif


/*------------------------------------------------------------------
 *                  pre-2.6.14/15 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * Get Free Pages flags
 *------------------------------------------------------------------*/

#if ((LINUX_VERSION_CODE < KERNEL_VERSION(2,6,14)) &&	\
     !defined(ESRFVER_ESRFLINUX_1_2))
typedef unsigned int gfp_t;
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,15)
#define GFP_DMA32		0
#endif


/*------------------------------------------------------------------
 *                  pre-2.6.18 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * IRQ handler definition
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#ifdef RHEL_RELEASE_CODE
#if RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(4, 6)
#define ESRFVER_NO_IRQ_HANDLER_T
#endif
#else
#define ESRFVER_NO_IRQ_HANDLER_T
#endif
#endif

#ifdef ESRFVER_NO_IRQ_HANDLER_T
typedef irqreturn_t (*irq_handler_t)(int, void *, struct pt_regs *);
#endif


/*------------------------------------------------------------------
 * Mutexes
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#include <asm/semaphore.h>

struct mutex {
	struct semaphore sem;
};

static inline void mutex_init(struct mutex *m)
{
	init_MUTEX(&m->sem);
}

static inline void mutex_lock(struct mutex *m)
{
	down(&m->sem);
}

static inline int mutex_lock_interruptible(struct mutex *m)
{
	return down_interruptible(&m->sem);
}

static inline void mutex_unlock(struct mutex *m)
{
	up(&m->sem);
}

#else // >= 2.6.18
#include <linux/mutex.h>
#endif


/*------------------------------------------------------------------
 *                  pre-2.6.19 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * kmem_cache_destroy return type
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
#define ESRFVER_KMEM_CACHE_DESTROY_RETURN
#endif

/*------------------------------------------------------------------
 * IRQ handler generic declaration/definition
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18)
#define IRQF_DISABLED		SA_INTERRUPT
#define IRQF_SAMPLE_RANDOM	SA_SAMPLE_RANDOM
#define IRQF_SHARED		SA_SHIRQ
#define IRQF_PROBE_SHARED	SA_PROBEIRQ
#define IRQF_PERCPU		SA_PERCPU
#endif

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,19)
#define DECLARE_IRQ_HANDLER(name, irq, data) \
	irqreturn_t name(int irq, void * data, struct pt_regs * pt_regs)
#else
#define DECLARE_IRQ_HANDLER(name, irq, data) \
	irqreturn_t name(int irq, void * data)
#endif

#define DEFINE_IRQ_HANDLER(name, irq, data) \
	DECLARE_IRQ_HANDLER(name, irq, data)


/*------------------------------------------------------------------
 *                  pre-2.6.32 compability
 *------------------------------------------------------------------*/

/*------------------------------------------------------------------
 * struct kmem_cache vs kmem_cache_t
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,23)
#define ESRFVER_HAS_KMEM_CACHE_T
#define ESRFVER_KMEM_CACHE_CTOR_DTOR NULL, NULL

#define ESRFVER_UNREGISTER_CHRDEV_RET
#else
#define ESRFVER_KMEM_CACHE_CTOR_DTOR NULL
typedef struct kmem_cache kmem_cache_t;
#endif

/*------------------------------------------------------------------
 * scatter_gather list management
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
#define sg_set_page(sg, pg, len, off)	   \
	do {				   \
		(sg)->page = (pg);	   \
		(sg)->length = (len);	   \
		(sg)->offset = (off);	   \
	} while (0)
#endif

/*------------------------------------------------------------------
 * DMA mapping
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,24)
#define DMA_BIT_MASK(n)	(((n) == 64) ? ~0ULL : ((1ULL<<(n))-1))
#endif

/*------------------------------------------------------------------
 * struct class_device vs device
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26)
#define ESRFVER_CLASS_DEV_STC
#define ESRFVER_CLASS_DEV_TYPE struct class_device
#define ESRFVER_CLASS_DEV_NAME(cls_dev) ((cls_dev)->class_id)
#define ESRFVER_CLASS_DEV_SET_NAME(cls_dev, name, ... ) \
	sprintf(ESRFVER_CLASS_DEV_NAME(cls_dev), name,  ## __VA_ARGS__)
#define ESRFVER_CLASS_DEV_ATTR_TYPE struct class_device_attribute
#define ESRFVER_CLASS_DEV_INITIALIZE(cls_dev) \
	class_device_initialize(cls_dev)
#define ESRFVER_CLASS_DEV_PUT(cls_dev) \
	class_device_put(cls_dev)
#define ESRFVER_CLASS_DEV_SET_DEV(cls_dev, d)	\
	(cls_dev)->dev = (d)
#define ESRFVER_CLASS_DEV_ADD(cls_dev) \
	class_device_add(cls_dev)
#define ESRFVER_CLASS_DEV_DEL(cls_dev) \
	class_device_del(cls_dev)
#define ESRFVER_CLASS_DEV_REGISTER(cls_dev) \
	class_device_register(cls_dev)
#define ESRFVER_CLASS_DEV_UNREGISTER(cls_dev) \
	class_device_unregister(cls_dev)
#define ESRFVER_CLASS_DEV_SET_DATA(cls_dev, data)	\
	class_set_devdata(cls_dev, data)
#define ESRFVER_CLASS_DEV_GET_DATA(cls_dev) \
	class_get_devdata(cls_dev)
#else
#define ESRFVER_CLASS_DEV_TYPE struct device
#define ESRFVER_CLASS_DEV_NAME(cls_dev) dev_name(cls_dev)
#define ESRFVER_CLASS_DEV_SET_NAME(cls_dev, name, ...) \
	dev_set_name(cls_dev, name, ## __VA_ARGS__)
#define ESRFVER_CLASS_DEV_ATTR_TYPE struct device_attribute
#define ESRFVER_CLASS_DEV_INITIALIZE(cls_dev) \
	device_initialize(cls_dev)
#define ESRFVER_CLASS_DEV_PUT(cls_dev) \
	kobject_put(&cls_dev->kobj)
#define ESRFVER_CLASS_DEV_SET_DEV(cls_dev, d)	\
	(cls_dev)->parent = (d)
#define ESRFVER_CLASS_DEV_ADD(cls_dev) \
	device_add(cls_dev)
#define ESRFVER_CLASS_DEV_DEL(cls_dev) \
	device_del(cls_dev)
#define ESRFVER_CLASS_DEV_REGISTER(cls_dev) \
	device_register(cls_dev)
#define ESRFVER_CLASS_DEV_UNREGISTER(cls_dev) \
	device_unregister(cls_dev)
#define ESRFVER_CLASS_DEV_SET_DATA(cls_dev, data)	\
	dev_set_drvdata(cls_dev, data)
#define ESRFVER_CLASS_DEV_GET_DATA(cls_dev) \
	dev_get_drvdata(cls_dev)
#endif

/*------------------------------------------------------------------
 * pci_clear_master 
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,29)
#ifdef RHEL_RELEASE_CODE
#if RHEL_RELEASE_CODE < RHEL_RELEASE_VERSION(5, 5)
#define ESRFVER_NO_PCI_CLEAR_MASTER
#endif
#else
#define ESRFVER_NO_PCI_CLEAR_MASTER
#endif
#endif

#ifdef ESRFVER_NO_PCI_CLEAR_MASTER

#include <linux/pci.h>

static inline
void pci_clear_master(struct pci_dev *dev)
{
	u16 cmd;
        pci_read_config_word(dev, PCI_COMMAND, &cmd);
        if ((cmd & PCI_COMMAND_MASTER) != 0) {
                cmd &= ~PCI_COMMAND_MASTER;
                pci_write_config_word(dev, PCI_COMMAND, cmd);
        }
}

#endif


/*------------------------------------------------------------------
 *                  pre-3.11 compability
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(3,11,0)
#define ESRFVER_CLASS_DEV_ATTRS

#define get_num_physpages()	num_physpages
#endif


/*------------------------------------------------------------------
 *                  pre-4.14 compability
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(4,14,0)

#define ESRFVER_TIMER_SETUP(timer, cb, flags) \
	do { \
		init_timer(timer); \
		(timer)->function = cb; \
		(timer)->data = (unsigned long) timer; \
	} while (0)

#define	ESRFVER_TIMER_FUNCTION(name, data) \
	void name(unsigned long data)
#define	ESRFVER_TIMER_FUNCTION_TIMER(data) \
	((struct timer_list *) (data))

#else

#define ESRFVER_TIMER_SETUP(timer, cb, flags) \
	timer_setup(timer, cb, flags)

#define	ESRFVER_TIMER_FUNCTION(name, data) \
	void name(struct timer_list *data)
#define	ESRFVER_TIMER_FUNCTION_TIMER(data) \
	(data)

#endif

/*------------------------------------------------------------------
 *                  pre-5.0 compability
 *------------------------------------------------------------------*/

#if LINUX_VERSION_CODE < KERNEL_VERSION(5,0,0)
#define ESRFVER_ACCESS_OK(ptr, len) access_ok(VERIFY_WRITE, ptr, len)
#else
#define ESRFVER_ACCESS_OK(ptr, len) access_ok(ptr, len)
#endif

/*------------------------------------------------------------------
 * Global errno
 *------------------------------------------------------------------*/

#define ESRFVER_NEEDS_GLOBLAL_ERRNO(uses_kthread, uses_sched_setsched) \
	(((uses_kthread) && defined(ESRFVER_KTHREAD_V1)) || \
         ((uses_sched_setsched) && defined(ESRFVER_SCHED_SET_SCHED_SYSCALL)))

#define ESRFVER_DEFINE_GLOBAL_ERRNO	int errno = 0

#endif /* _ESRFVERSION_H */
