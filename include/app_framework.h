/****************************************************************************
 * File:	app_framework.h
 * $Header: /segfs/bliss/cvs/driver/ESRF/common/include/app_framework.h,v 1.9 2009/03/16 14:35:53 ahoms Exp $
 * Project:	ESRF Linux driver framework
 * Description:	Library test program framework header
 * Author(s):	A. Homs, BLISS, ESRF (ahoms@esrf.fr)
 *
 * $Log: app_framework.h,v $
 * Revision 1.9  2009/03/16 14:35:53  ahoms
 * Added context_cleanup
 *
 * Revision 1.8  2009/02/09 19:46:24  kirov
 * + new command to allow users to add custom lines in the EDF file header
 * + the image library is initialized only when it is going to be used
 *
 * Revision 1.7  2007/02/14 10:36:13  ahoms
 * Alloc also initializes the context, so out/err can be used immediately.
 * Modified init_context to return an error if app functions are invalid.
 *
 * Revision 1.6  2007/02/01 14:14:17  ahoms
 * Using implementation-independ. type app_context_t, added alloc/free functions
 * Using out/err output functions; flush on every access.
 * Added program arguments to init_context; decode --interactive argument.
 *
 * Revision 1.5  2007/01/13 12:30:38  ahoms
 * Added system Shell function
 *
 * Revision 1.4  2006/10/23 09:01:13  ahoms
 * Modified quit_app to properly end the application
 *
 * Revision 1.3  2006/10/06 17:53:51  ahoms
 * Increased the input buffer length to 1024 bytes
 *
 * Revision 1.2  2006/10/03 08:33:51  ahoms
 * Support main loop poll functions by means of a separate input thread.
 * Added optional parameters and long command help description.
 * Added Sleep command with usec resolution; added generic dump_data helper.
 * Added readline functionality, including persistent history.
 * Allow several commands in the same line separated by ";".
 *
 * Revision 1.1  2005/08/21 23:47:12  ahoms
 * Help menu works with long parameter list functions
 *
 * Revision 1.0  2005/08/14 14:00:14  ahoms
 * Initial revision
 *
 ****************************************************************************/

#ifndef _APP_FRAMEWORK_H
#define _APP_FRAMEWORK_H

#include <stdio.h>
#include <pthread.h>

enum {
	PARAM_CHAR,
	PARAM_INT,
	PARAM_FLOAT,
	PARAM_STR,
	NR_PARAM_TYPES
};

#define PARAM_OPT		0x8000
#define PARAM_TYPE_MASK		0x7fff
#define PARAM_CHAR_OPT		(PARAM_CHAR  | PARAM_OPT)
#define PARAM_INT_OPT		(PARAM_INT   | PARAM_OPT)
#define PARAM_FLOAT_OPT		(PARAM_FLOAT | PARAM_OPT)
#define PARAM_STR_OPT		(PARAM_STR   | PARAM_OPT)

#define MAX_NR_PARAM		10
#define MAX_STR_LEN		1024

#define CONTEXT_INVALID		NULL

struct app_context;
union  param;
struct funct_param;
struct funct_data;

typedef struct app_context *app_context_t;

typedef int   (app_funct) (app_context_t context, union param *pars);
typedef char *(err_funct) (app_context_t context, struct funct_data *fdata, 
			   int ret);
typedef void  (poll_funct)(app_context_t context);

union param {
	char	 	 c;
	int	 	 i;
	float	 	 f;
	struct {
		char	 s[MAX_STR_LEN];
		int	 len;
	};
};

#define PARAM_OPT_NOT_PRESENT	"Optional Parameter Not Present"

#define set_opt_param_not_present(p) \
	do { \
		strcpy((p)->s, PARAM_OPT_NOT_PRESENT); \
		(p)->len = strlen((p)->s); \
	} while (0)

#define chk_opt_param_present(p) \
	(strcmp((p)->s, PARAM_OPT_NOT_PRESENT) != 0)

#define get_param_str(p) \
	({ \
		int l = (p)->len; \
		if (l >= MAX_STR_LEN) \
			l = MAX_STR_LEN - 1; \
		(p)->s[l] = 0; \
		(p)->s; \
	 })
	
struct funct_param {
	char	*name;
	int	 type;
	char	*desc;
};

struct funct_data {
	char			*name;
	app_funct		*funct;
	char			*desc;
	struct funct_param	params[MAX_NR_PARAM];
};


/*--------------------------------------------------------------------------
 * function prototypes
 *--------------------------------------------------------------------------*/

int is_interactive(app_context_t context);

app_context_t alloc_context(char *name, int argc, char *argv[]);
void free_context(app_context_t context);

int init_context(app_context_t context, struct funct_data *app_funcs, 
		 err_funct *err_funct, void *data);
int init_context_poll(app_context_t context, poll_funct **poll_funcs,
		      int nr_poll_funcs, int poll_sleep_usec);
void cleanup_context(app_context_t context);
int  set_poll_sleep_time(app_context_t context, int poll_sleep_usec);

void *context_app_data(app_context_t context);

int context_out(app_context_t context, char *format, ...);
int context_err(app_context_t context, char *format, ...);

char *bin_repr(char *buffer, unsigned long val);
int conv_param(char *token, struct funct_param *fparam, union param *val);
int context_get_line(app_context_t context, char *buffer, int len);
int get_funct_params(app_context_t context, char *line, 
		     struct funct_data *fdata, union param *pars);
int is_cmd(char *input, char *command);
int exec_application(app_context_t context);

int dump_data(app_context_t context, void *data, int len);

app_funct print_help;
app_funct quit_app;
app_funct run_script;
app_funct do_sleep;
app_funct system_shell;

void exact_usleep(long usec);


#endif /* _APP_FRAMEWORK_H */
