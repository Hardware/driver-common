# $Header: /segfs/bliss/cvs/driver/ESRF/common/include/makercs.inc,v 1.3 2007/01/27 19:30:38 ahoms Exp $

# This make include file defines the necessary variables for RCS 
# management. 
# See the maketarget.inc for the variables that must be defined in the 
# Makefile itself

ifdef VERSION
REVISION        = -r$(VERSION)
endif

ifdef LOCKMSG
RCSMSG          = -m"$(LOCKMSG)"
endif

RCSLOCK         = co -l $(REVISION)
RCSUNLOCK	= co -u $(REVISION)
RCSCO           = co $(REVISION)
RCSCI           = ci -u -f -s"Rel" $(REVISION) $(RCSMSG)
RCSDIFF		= rcsdiff $(DIFFOPT) $(REVISION)
TKDIFF		= tkdiff $(DIFFOPT) $(REVISION)

ifndef LINUX_VER
LINUX_VER       = $(shell uname -r | cut -d. -f1-2)
endif

MERGESCRIPTDIR	= /segfs/bliss/source/driver/linux-$(LINUX_VER)/include
MERGESCRIPTNAME	= mergedirnames

UNIX2DOSALL	= for f in $(RCSALL); do                \
                        tf=/tmp/myu2d_`basename $$f`;   \
                        mv $$f $$tf;                    \
                        unix2dos -k -n $$tf $$f;        \
                        chmod --reference $$tf $$f;     \
                        rm -f $$tf;                     \
                  done


