#
#  Makefile include for common target definition for ESRF drivers.
#  The copy2bin functionality separates binaries depending on the
#  compiling platform (OS, esrflinux ARCH, TARGET_KERN). It is
#  activated by default inside the ESRF and deactivated outside.
#
#  The file makeplat.inc must be included first, and this must be
#  included at the end.
#
#  $Header: /segfs/bliss/cvs/driver/ESRF/common/include/maketarget.inc,v 1.9 2007/01/27 19:30:38 ahoms Exp $
#

ifeq ($(COPYBIN), YES)

MOD_EXISTS	:= $(shell test -d $(MOD_DIR) && echo YES)
BIN_EXISTS	:= $(shell test -d $(BIN_DIR) && echo YES)
LIB_EXISTS	:= $(shell test -d $(LIB_DIR) && echo YES)

copybin:
ifdef DRV_MODULES
ifneq ($(MOD_EXISTS), YES)
	mkdir -p $(MOD_DIR)
endif
	cp $(DRV_MODULES) $(MOD_DIR)
endif
ifdef DRV_BIN
ifneq ($(BIN_EXISTS), YES)
	mkdir -p $(BIN_DIR);
endif
	cp $(DRV_BIN) $(BIN_DIR)
endif
ifdef DRV_LIB
ifneq ($(LIB_EXISTS), YES)
	mkdir -p $(LIB_DIR);
endif
	cp $(DRV_LIB) $(LIB_DIR)
endif
	@echo copybin done

cleanbin:
	rm -f $(MOD_DIR)/* $(BIN_DIR)/* $(LIB_DIR)/*

mrproper:	clean
	rmdir -p $(MOD_DIR) $(BIN_DIR) $(LIB_DIR) 2> /dev/null || true

endif # COPYBIN


ifeq ($(COPY2DEPOT), YES)
DEPOT_MOD_EXISTS:= $(shell test -d $(DEPOT_MOD_DIR) && echo YES)
DEPOT_BIN_EXISTS:= $(shell test -d $(DEPOT_BIN_DIR) && echo YES)
DEPOT_LIB_EXISTS:= $(shell test -d $(DEPOT_LIB_DIR) && echo YES)

real_copy2depot:
ifdef DRV_MODULES
ifneq ($(DEPOT_MOD_EXISTS), YES)
	mkdir -p $(DEPOT_MOD_DIR)
endif
	cp $(DRV_MODULES) $(DEPOT_MOD_DIR)
endif
ifdef DRV_BIN
ifneq ($(DEPOT_BIN_EXISTS), YES)
	mkdir -p $(DEPOT_BIN_DIR);
endif
	cp $(DRV_BIN) $(DEPOT_BIN_DIR)
endif
ifdef DRV_LIB
ifneq ($(DEPOT_LIB_EXISTS), YES)
	mkdir -p $(DEPOT_LIB_DIR);
endif
	cp $(DRV_LIB) $(DEPOT_LIB_DIR)
endif
	@echo copy2depot done
	

endif # COPY2DEPOT


# RCS targets
ifdef RCSFILES
include $(DRIVER_BASE)/include/makercstarget.inc
endif

