#
#  Makefile for ESRF driver common code
#

INCL		= include
DRV_LIB		= src/app_framework.o
ifdef QTDIR
DRV_LIB		+=src/image.o
DRV_BIN		= src/image_test
endif

include $(INCL)/makeplat.inc


all:		objects $(MAKE_COPYBIN)

objects:
	make -C src all

clean:		$(CLEAN_COPYBIN)
	make -C src clean


include $(INCL)/maketarget.inc
include $(INCL)/makecvstarget.inc
